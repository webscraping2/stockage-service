package fr.webscrapping.storage;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import io.vertx.core.json.JsonObject;
import com.mongodb.client.MongoClient;

@ApplicationScoped
public class JobService {
    @Inject
    MongoClient mongoClient;

    @Incoming("jobs")
    public void persistReceivedData(final JsonObject job) {
        final JobEntity jobEntity = new JobEntity();

        jobEntity.title = job.getString("title");
        jobEntity.company = job.getString("company");
        jobEntity.contract = job.getString("contract");
        jobEntity.location = job.getString("location");
        jobEntity.date = job.getString("date");

        jobEntity.persist();
    }
}
