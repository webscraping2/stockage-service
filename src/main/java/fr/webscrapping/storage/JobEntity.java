package fr.webscrapping.storage;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;

@MongoEntity(collection = "jobs")
public class JobEntity extends PanacheMongoEntity {
    public String title;
    public String company;
    public String contract;
    public String location;
    public String date;
}